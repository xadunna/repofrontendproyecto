import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-ajax/iron-ajax.js';
import '@vaadin/vaadin-text-field/vaadin-email-field.js';
import '@vaadin/vaadin-text-field/vaadin-password-field.js';
import '@vaadin/vaadin-button/vaadin-button.js';
import '@vaadin/vaadin-notification/vaadin-notification.js';


/**
 * @customElement
 * @polymer
 */
class LoginUsuario extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
        }
        .custom-style {
          width: 20em;
        }
      </style>


      <h3>Por favor introzca los datos</h3>
      <vaadin-email-field
        class="custom-style"
        label="Email"
        value="{{email::input}}"
        error-message="Introduzca una dirección de email válida"
        clear-button-visible
        required="true"
      >
      </vaadin-email-field>
      <br/>
      <vaadin-password-field
        class="custom-style"
        label="Password"
        value="{{password::input}}"
        required="true"
        clear-button-visible
        >
      </vaadin-password-field>
      <br/>

      <vaadin-button on-click="login">Iniciar sesión</vaadin-button>
      <vaadin-notification id="notification"  hidden="true" position="top-stretch" duration="4000">
      </vaadin-notification>
      <iron-ajax
        id="doLogin"
        url="http://localhost:3000/backend_banco/login"
        handle-as="json"
        content-type="application/json"
        method="POST"
        on-response="manageAJAXresponse"
        on-error="showError"
      >
      </iron-ajax>
    `;
  }
  static get properties() {
    return {
      email: {
        type: String,
      },
      password: {
        type: String
      }
    };
  }// End properties

  login(){
    if(this.email!=null && this.password!=null && this.email!="" && this.password!=""){

      var loginData = {
          "email": this.email,
          "password": this.password
      }
      console.log("Lanzamos petición de login");
      //Con el $ referencio a un componente que está dentro del html
      this.$.doLogin.body=JSON.stringify(loginData); //la variable en memoria es binaria
      this.$.doLogin.generateRequest(); //lanza la petición
    }else{
      this.lanzarNotificacion("Por favor, rellene todos los campos");
    }
  }

  //100,200 y 300
  manageAJAXresponse(data){
    console.log("Llegaron los resultados de login-usario");
    //lanzamos evento con id de usuario
    this.email = null;
    this.password = null;
    window.sessionStorage.setItem('token', 'Bearer ' + data.detail.response.token);
    console.log("Lanzamos evento desde login-usuario");
    this.dispatchEvent(
          new CustomEvent(
            "login",
            {
              "detail" : {
                "idusuario": data.detail.response.id
              }//hasta aquí la estructura es fija
            }//object siempre es CustomEvent
          )//customEvent
        )//dispatch
  }

  showError(error){
    console.log("Hubo un error");
    console.log(error.detail.request.xhr.response.msg);    
    this.lanzarNotificacion(error.detail.request.xhr.response.msg);
  }

  lanzarNotificacion(texto){
    this.$.notification.renderer = function(root) {
              root.textContent = texto;
            };
    this.$.notification.hidden = false;
    this.$.notification.opened = true;
  }

}//End class

window.customElements.define('login-usuario', LoginUsuario);
