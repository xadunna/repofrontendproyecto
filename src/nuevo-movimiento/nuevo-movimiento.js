import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-ajax/iron-ajax.js';
import '@vaadin/vaadin-combo-box/vaadin-combo-box.js';
import '@vaadin/vaadin-text-field/vaadin-text-field.js';
import '@vaadin/vaadin-button/vaadin-button.js';
import '@vaadin/vaadin-text-field/vaadin-number-field.js';
import '@vaadin/vaadin-notification/vaadin-notification.js';



/**
 * @customElement
 * @polymer
 */
class NuevoMovimiento extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block
        }
        .custom-style {
          width: 15em;
        }
      </style>
      <h3>Por favor, introduzca los datos</h3>
      <vaadin-combo-box id="combo"
        class="custom-style"
        label="Tipo de movimiento"
        items="[[tipos_movimientos]]"
        on-change="definirMovimiento"
        required="true"
        >
      </vaadin-combo-box>
      <br/>
      <vaadin-number-field
        class="custom-style"
        label="Importe (1.00-2000)"
        value="{{importe::input}}"
        placeholder="00.00"
        pattern="[0-9]+([\.][0-9]+{0,2})?"
        step="0.01"
        min="1.00"
        max="2000"
        required="true"
        error-message="Introduzca un valor válido"
        clear-button-visible
      >
      </vaadin-number-field>
      <br/>
      <vaadin-text-field
        class="custom-style"
        label="Descripción"
        value="{{descripcion::input}}"
        clear-button-visible
      >
      </vaadin-text-field>
      <br/>
      <br/>
      <vaadin-button id="buttonCreateTransaction" on-click="crear">Crear movimiento</vaadin-button>
      <vaadin-button id="returnButton" on-click="volver">Volver</vaadin-button>
      <br/>
      <vaadin-notification id="notification"  hidden="true" position="top-stretch" duration="3000">
      </vaadin-notification>
      <iron-ajax
        id="newTransaction"
        url="http://localhost:3000/backend_banco/transactions"
        handle-as="json"
        content-type="application/json"
        method="POST"
        on-response="manageAJAXresponse"
        on-error="showError"
      >
      </iron-ajax>
    `;
  }
  static get properties() {
    return {
      accountid: {
        type: Number
      },
      tipoMovimiento: {
        type: String
      },
      descripcion: {
        type: String
      },
      importe: {
        type: Number
      },
      tipos_movimientos : {
        type: Array,
        value:["INGRESO","REINTEGRO"]
      }
    };
  }// End properties

    crear(){
      if(this.tipoMovimiento!=null && this.importe!=null){
        var newTransactionData = {
            "id_cuenta": this.accountid,
            "tipo_movimiento": this.tipoMovimiento,
            "importe": this.importe,
            "descripcion": this.descripcion
        }
        console.log("Lanzamos petición desde nuevo-movimiento");
        //Con el $ referencio a un componente que está dentro del html
        var token = window.sessionStorage.getItem('token');
        this.$.newTransaction.headers['Authorization'] = token;
        this.$.newTransaction.body=JSON.stringify(newTransactionData); //la variable en memoria es binaria
        this.$.newTransaction.generateRequest(); //lanza la petición
    } else{
      this.lanzarNotificacion("Por favor rellene todos los campos");
    }
  }

  definirMovimiento(){
    console.log("El usuario ha seleccionado un movimiento");
    this.tipoMovimiento = this.$.combo.value;
  }

  //100,200 y 300
  manageAJAXresponse(data){
    console.log("Nuevo movimiento creado");
    this.lanzarNotificacion(data.detail.response.msg);
    this.limpiarCamposPet();
    this.lanzarEvento();

  }

  showError(error){
    console.log("Hubo un error en nuevo-movimiento");
    console.log(error.detail.request.xhr.response.msg);    
    this.lanzarNotificacion(error.detail.request.xhr.response.msg);
    this.limpiarCamposPet();
  }

  volver(){
    this.limpiarCamposPet();
    this.limpiarCampos();
    this.lanzarEvento();
  }

  lanzarNotificacion(texto){
    this.$.notification.renderer = function(root) {
              root.textContent = texto;
            };
    this.$.notification.hidden = false;
    this.$.notification.opened = true;
  }

  lanzarEvento(){
    //lanzamos evento con accountid
    console.log("Lanzamos evento desde nuevo-movimiento");
    this.dispatchEvent(
          new CustomEvent(
            "new-transaction",
            {
              "detail" : {
              }//hasta aquí la estructura es fija
            }//object siempre es CustomEvent
          )//customEvent
        )//dispatch
  }

  limpiarCamposPet(){
    this.tipoMovimiento = null;
    this.importe = null;
    this.descripcion = null;
    this.$.combo.selectedItem = "";
  }

  limpiarCampos(){
    this.accountid = null;
  }

}//End class

window.customElements.define('nuevo-movimiento', NuevoMovimiento);
