import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-ajax/iron-ajax.js';
import '@polymer/iron-pages/iron-pages.js';
import '../dashboard-cuentas/dashboard-cuentas.js';
import '../dashboard-movimientos/dashboard-movimientos.js';
import '../dashboard-gestion-usuario/dashboard-gestion-usuario.js';
import '../cambio-divisa/cambio-divisa.js';
import '@vaadin/vaadin-button/vaadin-button.js';


/**
 * @customElement
 * @polymer
 */
class DashboardUsuario extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
        }
      </style>


      <h3 id="bienvenidoUsuario"></h3>

      <iron-pages selected="[[componentName]]" attr-for-selected="component-name">
        <div component-name="dashboard-gestion-usuario">
          <dashboard-gestion-usuario
          on-login-success="processLoginSuccess"
          >
          </dashboard-gestion-usuario>
        </div>
        <div component-name="dashboard-cuentas">
          <dashboard-cuentas id="dashboardCuentas"
            on-show-transactions="showTransactions"
          </dashboard-cuentas>
        </div>
        <div component-name="dashboard-movimientos">
          <dashboard-movimientos id="dashboardMovimientos"
            on-return-transactions="processReturnTransactions"
            >
          </dashboard-movimientos>
        </div>
        <div component-name="cambio-divisa">
          <cambio-divisa id="cambioDivisa"
            on-return-cambio-divisa="processCambioDivisa"
          >
          </cambio-divisa>
        </div>
      </iron-pages>

      <iron-ajax
       method="POST"
       id="logout"
       url="http://localhost:3000/backend_banco/logout/{{userid}}"
       handle-as="json"
       on-response="showDataLogout"
       on-error="showErrorLogout"
      >
      </iron-ajax>

      <iron-ajax
       method="GET"
       id="getUser"
       url="http://localhost:3000/backend_banco/users/{{userid}}"
       handle-as="json"
       on-response="showDataUser"
      >
      </iron-ajax>

      <vaadin-button id="currencyConverterButton" on-click="currencyConverter" hidden="true">Cambio de divisa</vaadin-button>
      <vaadin-button id="logoutButton" on-click="logout" hidden="true">Cerrar sesión</vaadin-button>
    `;
  }
  static get properties() {
    return {
      componentName: {
        type: String,
        value: "dashboard-gestion-usuario"
      },
      userid: {
        type: Number
      }
    };
  }// End properties

  processLoginSuccess(e){
    console.log("Capturado evento de dashboard-gestion-usuario");
    this.userid = e.detail.userid;
    this.$.dashboardCuentas.userid = this.userid;
    console.log("Lanzamos petición de datos de usuario desde visor-cuentas");
      //Con el $ referencio a un componente que está dentro del html
    var token = window.sessionStorage.getItem('token');
    this.$.getUser.headers['Authorization'] = token;
    this.$.getUser.userid=JSON.stringify(this.userid); //la variable en memoria es binaria
    this.$.getUser.generateRequest(); //lanza la petición
    this.componentName = "dashboard-cuentas";
    this.$.logoutButton. hidden = false;
    this.$.currencyConverterButton. hidden = false;
  }

  processReturnTransactions(e){
    console.log("Capturado evento de dashboard-movimientos");
    this.$.dashboardCuentas.updated = true;
    this.componentName = "dashboard-cuentas";
  }

  processCambioDivisa(e){
    console.log("Capturado evento de cambio-divisas");
    this.componentName = "dashboard-cuentas";
    this.$.currencyConverterButton.hidden = false;
  }

  showTransactions(e){
    console.log("Capturado evento de dashboard-cuentas");
    this.$.dashboardMovimientos.accountid = e.detail.accountid;
    this.$.dashboardMovimientos.iban = e.detail.iban;
    this.componentName = "dashboard-movimientos";

  }

  currencyConverter(){
    console.log("abrimos cambio de divisa");
    this.$.cambioDivisa.updated = true;
    this.componentName = "cambio-divisa";
    this.$.currencyConverterButton.hidden = true;
  }

  showDataUser(data){
    this.$.bienvenidoUsuario.innerHTML = "Bienvenido/a "+ data.detail.response.first_name + " " +  data.detail.response.last_name;
  }

  logout(){
    console.log("Lanzamos petición de logout");
    this.$.bienvenidoUsuario.innerHTML = null;
    var token = window.sessionStorage.getItem('token');
    this.$.logout.headers['Authorization'] = token;
    this.$.logout.userid=JSON.stringify(this.userid); //la variable en memoria es binaria
    this.$.logout.generateRequest();

  }

  showDataLogout(){
    window.sessionStorage.setItem('token',null);
    this.componentName = "dashboard-gestion-usuario";
    this.userid = null;
    this.$.logoutButton.hidden = true;
    this.$.currencyConverterButton.hidden = true;

  }

  showErrorLogout(){
    console.log("Hubo un error en el logout");

  }

}//End class

window.customElements.define('dashboard-usuario', DashboardUsuario);
