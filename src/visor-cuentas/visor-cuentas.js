
import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';

import '@polymer/iron-ajax/iron-ajax.js';
import '@vaadin/vaadin-grid/vaadin-grid.js';
import '@vaadin/vaadin-grid/vaadin-grid-column.js';
import '@vaadin/vaadin-notification/vaadin-notification.js';
/**
 * @customElement
 * @polymer
 */
class VisorCuentas extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
          /*all: initial;*/
        }

      </style>

      <vaadin-grid  id="grid" theme="row-dividers" items="{{accounts}}" active-item="{{selectedItem}}" hidden="true">
        <vaadin-grid-column width="9em" path="IBAN" header="Cuenta"></vaadin-grid-column>
        <vaadin-grid-column width="9em" path="balance" header="Saldo"></vaadin-grid-column>
      </vaadin-grid>

      <vaadin-notification id="notification"  hidden="true" position="top-stretch" duration="3000">
      </vaadin-notification>

      <iron-ajax
       method="GET"
       id="getAccount"
       url="http://localhost:3000/backend_banco/accounts/{{userid}}"
       handle-as="json"
       on-response="showData"
       on-error="showError"
      >
      </iron-ajax>
            `;
  }
  static get properties() {
    return {
      accounts: {
        type: Array
      },
      userid: {
        type: Number,
        observer: "_useridChanged"
      },
      selectedItem: {
        observer: "_onActiveItemChanged"
      },
      updated: {
        type: Boolean,
        observer: "_updatedChanged"
      }
    };
  } //End properties


//La función manejadora tiene un único argumento
 showData(data){
   console.log("Mostramos datos de visor-cuentas");
   this.$.grid.hidden = false;
   this.accounts = data.detail.response;
   this.updated = false;
}

showError(error){
  console.log("Hubo un error en visor-cuentas");
  console.log(error.detail.request.xhr.response.msg);
  this.$.notification.renderer = function(root) {
            root.textContent = error.detail.request.xhr.response.msg;
          };
  this.$.notification.hidden = false;
  this.$.notification.opened = true;
  this.$.grid.hidden = true;
  this.updated = false;
}



_useridChanged(oldValue, newValue)
{
     if (this.userid!=null){
       console.log("Tenemos usuario para hacer la petición de visor-cuentas");
       this.lanzarPeticion();
    }
}

_onActiveItemChanged(item){
  if(this.userid){
    this.$.grid.selectedItems = item ? [item] : [];
    //console.log(this.$.grid.selectedItems);
    if(this.$.grid.selectedItems.length >0){
      //lanzamos evento con idcuenta
      console.log("Lanzamos evento desde visor-cuentas");
      this.selectedItem = item;
      this.dispatchEvent(
            new CustomEvent(
              "activate-buttons",
              {
                "detail" : {
                  "accountid": item.id,
                  "iban": item.IBAN
                }//hasta aquí la estructura es fija
              }//object siempre es CustomEvent
            )//customEvent
          )//dispatch
          console.log("Mostramos botones de ver movimientos y de borrar cuenta");
    }else {
      console.log("Lanzamos evento desde visor-cuentas");
      this.dispatchEvent(
            new CustomEvent(
              "inactivate-buttons",
              {
                "detail" : {
                }//hasta aquí la estructura es fija
              }//object siempre es CustomEvent
            )//customEvent
          )//dispatch
         console.log("Ocultamos botones de ver movimientos y de borrar cuenta");
    }

  }
}

_updatedChanged(newValue, oldValue){

  if (newValue === true){
    console.log("updated de visor-cuentas cambió");
    this.updated = false;
    this.limpiarCampos();
    this.lanzarPeticion();
  }

}

lanzarPeticion(){
console.log("Lanzamos petición desde visor-cuentas");
  this.$.grid.hidden = true;
  //Con el $ referencio a un componente que está dentro del html
  var token = window.sessionStorage.getItem('token');
  this.$.getAccount.headers['Authorization'] = token;
  this.$.getAccount.userid=JSON.stringify(this.userid); //la variable en memoria es binaria
  this.$.getAccount.generateRequest(); //lanza la petición
}

limpiarCampos(){
  this.selectedItem = null;
  this.accounts = null;
  this.iban = null;
}

}// End class

window.customElements.define('visor-cuentas', VisorCuentas);
