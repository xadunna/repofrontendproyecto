import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-ajax/iron-ajax.js';
import '@vaadin/vaadin-text-field/vaadin-text-field.js';
import '@vaadin/vaadin-button/vaadin-button.js';
import '@vaadin/vaadin-notification/vaadin-notification.js';



/**
 * @customElement
 * @polymer
 */
class NuevaCuenta extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block
        }
        .custom-style {
          width: 20em;
        }
      </style>

      <h3>Por favor, introduzca los datos</h3>
      <vaadin-text-field id ="ibanText"
      class="custom-style"
      label="IBAN (ES00 0000 0000 0000 0000 0000)"
      value="{{iban::input}}"
      required="true"
      placeholder="ES00 0000 0000 0000 0000 0000"
      clear-button-visible
      maxlength="29"
       >
       </vaadin-text-field>
      <br/>
      <br/>
      <vaadin-button id="buttonCreateAccount" on-click="crear">Crear cuenta</vaadin-button>
      <vaadin-button id="returnButton" on-click="volver">Volver</vaadin-button>
      <br/>
      <vaadin-notification id="notification"  hidden="true" position="top-stretch" duration="3000">
      </vaadin-notification>
      <iron-ajax
        id="newAccount"
        url="http://localhost:3000/backend_banco/accounts"
        handle-as="json"
        content-type="application/json"
        method="POST"
        on-response="manageAJAXresponse"
        on-error="showError"
      >
      </iron-ajax>
    `;
  }
  static get properties() {
    return {
      iban: {
        type: String
      },
      userid: {
        type: Number
      }
    };
  }// End properties
    crear(){
    if(this.iban!=null) {
        var newAccountData = {
            "id_usuario": this.userid,
            "iban": this.iban
        }

        console.log("Lanzamos petición desde nueva-cuenta");
        //Con el $ referencio a un componente que está dentro del html
        var token = window.sessionStorage.getItem('token');
        this.$.newAccount.headers['Authorization'] = token;
        this.$.newAccount.body=JSON.stringify(newAccountData); //la variable en memoria es binaria
        this.$.newAccount.generateRequest(); //lanza la petición
    } else{
      this.lanzarNotificacion("Por favor, rellene todos los campos");
    }
  }

  //100,200 y 300
  manageAJAXresponse(data){
    console.log("Cuenta creada");
    this.lanzarNotificacion(data.detail.response.msg);
    this.limpiarCamposPet();
    this.lanzarEvento();
  }

  showError(error){
    console.log("Hubo un error en nueva-cuenta");
    console.log(error.detail.request.xhr.response.msg);    
    this.limpiarCamposPet();
    this.lanzarNotificacion(error.detail.request.xhr.response.msg);
  }

  lanzarNotificacion(texto){
    this.$.notification.renderer = function(root) {
              root.textContent = texto;
            };
    this.$.notification.hidden = false;
    this.$.notification.opened = true;
  }

  volver(){
    this.limpiarCamposPet();
    this.limpiarCampos();
    this.lanzarEvento();

  }

  lanzarEvento(){
    //lanzamos evento con iban
    console.log("Lanzamos evento desde nueva-cuenta");
    this.dispatchEvent(
          new CustomEvent(
            "new-account",
            {
              "detail" : {
              }//hasta aquí la estructura es fija
            }//object siempre es CustomEvent
          )//customEvent
        )//dispatch
  }

  limpiarCamposPet(){
    this.iban = null;
  }

  limpiarCampos(){
    this.userid = null;
  }

}//End class

window.customElements.define('nueva-cuenta', NuevaCuenta);
