
import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';

import '@polymer/iron-ajax/iron-ajax.js';
import '@vaadin/vaadin-grid/vaadin-grid.js';
import '@vaadin/vaadin-grid/vaadin-grid-column.js';
import '@vaadin/vaadin-notification/vaadin-notification.js';
/**
 * @customElement
 * @polymer
 */
class VisorMovimientos extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
          /*all: initial;*/
        }
      </style>

      <h3 id="accountText"></h3>
       <vaadin-grid id="grid" hidden="true" theme="row-dividers" items="{{transactions}}">
         <vaadin-grid-column width="9em" path="tipo_movimiento" header="Tipo de movimiento"></vaadin-grid-column>
         <vaadin-grid-column width="9em" path="fecha" header="Fecha"></vaadin-grid-column>
         <vaadin-grid-column width="9em" path="importe" header="Importe"></vaadin-grid-column>
         <vaadin-grid-column width="9em" path="saldo_inicial" header="Saldo inicial"></vaadin-grid-column>
         <vaadin-grid-column width="9em" path="saldo_final" header="Saldo final"></vaadin-grid-column-column>
       </vaadin-grid>


       <vaadin-notification id="notification"  hidden="true" position="top-stretch" duration="3000">
       </vaadin-notification>

      <iron-ajax
       id="getTransactions"
       url="http://localhost:3000/backend_banco/transactions/{{accountid}}"
       handle-as="json"
       on-response="showData"
       on-error="showError"
       method="GET"
      >
      </iron-ajax>
    `;
  }
  static get properties() {
    return {
      transactions: {
        type: Array
      },
      accountid: {
        type: Number
      },
      iban: {
        type: String
      },
      updated: {
        type: Boolean,
        observer: "_updatedChanged"
      }
    };
  } //End properties

  static get observers() {
       return [
         'hacerPetición(accountid, iban)'
       ];
     }

//La función manejadora tiene un único argumento
 showData(data){
   console.log("Mostramos datos de visor-movimientos");
   this.transactions = data.detail.response;
   this.$.grid.hidden = false;
   this.updated = false;
   this.$.accountText.innerHTML = "Movimientos de la cuenta: "+ this.iban;
}

showError(error){
  console.log("Hubo un error en visor-movimientos");
  console.log(error.detail.request.xhr.response.msg);  
  this.$.notification.renderer = function(root) {
            root.textContent = error.detail.request.xhr.response.msg;
          };
  this.$.notification.hidden = false;
  this.$.notification.opened = true;
  this.$.grid.hidden = true;
  this.updated = false;
  this.$.accountText.innerHTML = "";
}


hacerPetición(accountid, iban)
{
     if (this.accountid!=null && this.iban!=null ){
       console.log("Tenemos  cuenta e iban para hacer la petición de visor-movimientos");
       this.lanzarPeticion();
    }
    else{
      this.$.accountText.innerHTML = "";
    }
}

_updatedChanged(newValue, oldValue){
  if (newValue === true){
    console.log("updated de visor-movimientos cambió");
    this.updated = false;
    this.lanzarPeticion();
  }

}

lanzarPeticion(){
  //Con el $ referencio a un componente que está dentro del html
  console.log("Lanzamos petición desde visor-movimientos");
  this.$.grid.hidden = true;
  var token = window.sessionStorage.getItem('token');
  this.$.getTransactions.headers['Authorization'] = token;
  this.$.getTransactions.body = JSON.stringify(this.accountid); //la variable en memoria es binaria
  this.$.getTransactions.generateRequest(); //lanza la petición
}

}// End class

window.customElements.define('visor-movimientos', VisorMovimientos);
