import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-ajax/iron-ajax.js';
import '@polymer/iron-pages/iron-pages.js';
import '@vaadin/vaadin-button/vaadin-button.js';
import '@vaadin/vaadin-dialog/vaadin-dialog.js';
import '../visor-cuentas/visor-cuentas.js';
import '../nueva-cuenta/nueva-cuenta.js';




/**
 * @customElement
 * @polymer
 */
class DashboardCuentas extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
        }
      </style>

      <iron-pages selected="[[componentName]]" attr-for-selected="component-name">
        <div component-name="visor-cuentas">
          <visor-cuentas id="visorCuentas"
            userid="[[userid]]"
            on-activate-buttons="activateButtons"
            on-inactivate-buttons="inactivateButtons"
          >
          </visor-cuentas>
          </div>
        <div component-name="nueva-cuenta">
          <nueva-cuenta id="nuevaCuenta"
          userid="[[userid]]"
          on-new-account="refreshAccounts"
          >
          </nueva-cuenta></div>
      </iron-pages>
      <iron-ajax
       method="DELETE"
       id="deleteAccount"
       url="http://localhost:3000/backend_banco/accounts/{{accountid}}"
       handle-as="json"
       on-response="showDataDeleteAccount"
       on-error="showErrorDeleteAccount"
      >
      </iron-ajax>

      <vaadin-notification id="notification"  hidden="true" position="bottom-stretch" duration="4000">
      </vaadin-notification>
      <vaadin-button id="createAccountButton" on-click="createAccount">Crear cuenta</vaadin-button>
      <vaadin-button id="deleteAccountButton" on-click="deleteAccount"  disabled="true">Borrar cuenta</vaadin-button>
      <vaadin-button id="transactionsButton" on-click="showTransactions" disabled="true">Ver movimientos</vaadin-button>
      <vaadin-dialog id="confirmationDialog"no-close-on-esc no-close-on-outside-click hidden="true">
      </vaadin-dialog>
    `;
  }
  static get properties() {
    return {
      componentName: {
        type: String,
        value: "visor-cuentas"
      },
      userid: {
        type: Number
      },
      accountid: {
        type: Number
      },
      iban: {
        type: String
      },
      updated: {
        type: Boolean,
        observer: "_updatedChanged"
      }
    };
  }// End properties


  createAccount(){
    this.componentName = "nueva-cuenta";
    this.desactivarBotones();
    this.esconderBotones();
  }

  deleteAccount(){
    var dialog = this.$.confirmationDialog;
    var peticion = this.$.deleteAccount;
    dialog.renderer = function(root, dialog) {
  // Check if there is a DOM generated with the previous renderer call to update its content instead of recreation
      if (root.firstElementChild) {
        return;
      }
      var div = window.document.createElement('div');
      div.textContent = 'Se borraran los movimientos asociados, ¿desea continuar?';

      var br = window.document.createElement('br');

      var okButton = window.document.createElement('vaadin-button');
      okButton.setAttribute('theme', 'primary');
      okButton.textContent = 'Aceptar';
      okButton.setAttribute('style', 'margin-right: 1em');
      okButton.addEventListener('click', function() {
        console.log("Lanzamos petición para borrar cuentas");
        dialog.opened = false;
        var token = window.sessionStorage.getItem('token');
        peticion.headers['Authorization'] = token;
        peticion.generateRequest();
      });

      var cancelButton = window.document.createElement('vaadin-button');
      cancelButton.textContent = 'Cancelar';
      cancelButton.addEventListener('click', function() {
            dialog.opened = false;
      });

      root.appendChild(div);
      root.appendChild(br);
      root.appendChild(okButton);
      root.appendChild(cancelButton);
    };
  this.$.confirmationDialog.opened = true;
  this.desactivarBotones();

  }

  showDataDeleteAccount(data){
    console.log("Cuenta borrada");
    this.$.visorCuentas.updated = true;
    this.componentName = "visor-cuentas";
  }

  showErrorDeleteAccount(error){
    console.log("Hubo un error en dashboard-cuentas: borrar");
    this.$.notification.renderer = function(root) {
              root.textContent = error.detail.request.xhr.response.msg;
            };
    this.$.notification.hidden = false;
    this.$.notification.opened = true;
  }

  showTransactions(){
    console.log("Botón de ver movimientos");
    this.dispatchEvent(
          new CustomEvent(
            "show-transactions",
            {
              "detail" : {
                "accountid": this.accountid,
                "iban": this.iban
              }//hasta aquí la estructura es fija
            }//object siempre es CustomEvent
          )//customEvent
        )//dispatch
    this.desactivarBotones();
  }

  activateButtons(e){
    this.activarBotones();
    this.accountid = e.detail.accountid;
    this.iban = e.detail.iban;
  }

  inactivateButtons(e){
    this.desactivarBotones();
    this.accountid = null;
    this.iban = null;
  }

  _updatedChanged(newValue, oldValue){
    console.log("update de dashboard-cuentas cambió");
    if(newValue === true){
      this.$.visorCuentas.updated = true;
      this.componentName = "visor-cuentas";
      this.updated = false;
    }
  }

  refreshAccounts(e){
    this.$.visorCuentas.updated = true;
    this.componentName = "visor-cuentas";
    this.mostrarBotones();
  }

  esconderBotones(){
    this.$.createAccountButton.hidden = true;
    this.$.deleteAccountButton.hidden = true;
    this.$.transactionsButton.hidden = true;
  }

  mostrarBotones(){
    this.$.createAccountButton.hidden = false;
    this.$.deleteAccountButton.hidden = false;
    this.$.transactionsButton.hidden = false;
  }

  activarBotones(){
    this.$.transactionsButton.disabled = false;
    this.$.deleteAccountButton.disabled = false;
  }

  desactivarBotones(){
    this.$.transactionsButton.disabled = true;
    this.$.deleteAccountButton.disabled = true;
  }

}//End class

window.customElements.define('dashboard-cuentas', DashboardCuentas);
