import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-ajax/iron-ajax.js';
import '@polymer/iron-pages/iron-pages.js';
import '../visor-movimientos/visor-movimientos.js';
import '../nuevo-movimiento/nuevo-movimiento.js';



/**
 * @customElement
 * @polymer
 */
class DashboardMovimientos extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
        }
      </style>

      <iron-pages selected="[[componentName]]" attr-for-selected="component-name">
        <div component-name="visor-movimientos">
          <visor-movimientos id="visorMovimientos"
            accountid="[[accountid]]"
            iban="[[iban]]"
            >
          </visor-movimientos>
         </div>
        <div component-name="nuevo-movimiento">
          <nuevo-movimiento id="nuevoMovimiento"
            accountid="[[accountid]]"
            on-new-transaction="processNewTransaction"
          >
          </nuevo-movimiento>
        </div>
      </iron-pages>

      <vaadin-button id="newTransactionButton" on-click="newTransaction">Nuevo movimiento</vaadin-button>
      <vaadin-button id="returnButton" on-click="volver">Volver</vaadin-button>
    `;
  }
  static get properties() {
    return {
      componentName: {
        type: String,
        value: "visor-movimientos"
      },
      userid: {
        type: Number
      },
      accountid: {
        type: Number
      },
      iban: {
        type: String
      }
    };
  }// End properties


  static get observers() {
         return [
           'mostrarMovimientos(accountid, iban)'
         ];
   }
  processNewTransaction(e){
    console.log("Capturado evento de nuevo-movimiento");
    this.$.visorMovimientos.updated = true;
    this.componentName = "visor-movimientos";
    this.mostrarBotones();
  }

  mostrarMovimientos(accountid, iban){
    if(this.accountid!= null && this.iban!=null){
    this.componentName = "visor-movimientos";
    }
  }

  newTransaction(){
    console.log("Vamos a añadir un movimiento");
    this.componentName = "nuevo-movimiento";
    this.esconderBotones();

  }
  esconderBotones(){
    this.$.newTransactionButton.hidden = true;
    this.$.returnButton.hidden = true;
  }

  mostrarBotones(){
    this.$.newTransactionButton.hidden = false;
    this.$.returnButton.hidden = false;
  }

  volver(){
    console.log("Lanzamos evento de volver desde dashboard-movimientos");
    this.dispatchEvent(
          new CustomEvent(
            "return-transactions",
            {
              "detail" : {
              }//hasta aquí la estructura es fija
            }//object siempre es CustomEvent
          )//customEvent
        )//dispatch
    this.limpiarCampos();
  }

  limpiarCampos(){
    this.userid = null;
    this.accountid = null;
    this.iban = null;
  }

}//End class

window.customElements.define('dashboard-movimientos', DashboardMovimientos);
