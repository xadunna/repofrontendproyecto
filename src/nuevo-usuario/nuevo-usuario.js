import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-ajax/iron-ajax.js';
import '@vaadin/vaadin-text-field/vaadin-email-field.js';
import '@vaadin/vaadin-text-field/vaadin-password-field.js';
import '@vaadin/vaadin-text-field/vaadin-text-field.js';
import '@vaadin/vaadin-button/vaadin-button.js';
import '@vaadin/vaadin-notification/vaadin-notification.js';

/**
 * @customElement
 * @polymer
 */
class NuevoUsuario extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
        }
        .custom-style {
          width: 20em;
        }
      </style>

      <h3>Por favor, introduzca los datos</h3>
      <vaadin-text-field class="custom-style" label="Nombre" value="{{nombre::input}}" placeholder="Nombre" required="true"></vaadin-text-field>
      <br/>
      <vaadin-text-field  class="custom-style" label="Apellidos" value="{{apellidos::input}}" placeholder="Apellidos" required="true"></vaadin-text-field>
      <br/>
      <vaadin-email-field class="custom-style" label="Email" value="{{email::input}}" placeholder="email@email.com" required="true" ></vaadin-email-field>
      <br/>
      <vaadin-password-field class="custom-style" label="Contraseña" value="{{password::input}}" required="true"></vaadin-password-field>
      <br/>
      <vaadin-password-field class="custom-style" label="Confirme contraseña" value="{{passwordRequired::input}}" required="true"></vaadin-password-field>
      <br/>
      <iron-ajax
        id="newUser"
        url="http://localhost:3000/backend_banco/users"
        handle-as="json"
        content-type="application/json"
        method="POST"
        on-response="manageAJAXresponse"
        on-error="showError"
      >
      </iron-ajax>
      <vaadin-button id="buttonCreateUser" on-click="registro">Registrarse</vaadin-button>
      <vaadin-button id="returnButton" on-click="volver">Volver</vaadin-button>
      <br/>
      <vaadin-notification id="notification"  hidden="true" position="top-stretch" duration="3000">
      </vaadin-notification>
    `;
  }
  static get properties() {
    return {
      nombre: {
        type: String
      },
      apellidos: {
        type: String
      },
      email: {
        type: String
      },
      password: {
        type: String
      },
      passwordRequired: {
        type: String
      },
      userid: {
        type: String
      }
    };
  }// End properties

    registro(){

    if(this.nombre!=null && this.apellidos!=null && this.email!=null &&
      this.password!=null && this.passwordRequired!=null){

      if(this.password == this.passwordRequired) {
        var newUserData = {
            "first_name": this.nombre,
            "last_name": this.apellidos,
            "email": this.email,
            "password": this.password
        }
        console.log("Lanzamos petición desde nuevo-usuario");
        //Con el $ referencio a un componente que está dentro del html
        this.$.newUser.body=JSON.stringify(newUserData); //la variable en memoria es binaria
        this.$.newUser.generateRequest(); //lanza la petición
    } else{
      console.log("Las contraseñas no coinciden");
      this.lanzarNotificacion("Las contraseñas no coinciden");
      }
    } else{
      this.lanzarNotificacion("Por favor rellene todos los campos");
    }
  }

  //100,200 y 300
  manageAJAXresponse(data){
    console.log("Usuario creado");
    this.userid = data.detail.response.id;
    this.lanzarNotificacion(data.detail.response.msg);
    //lanzamos evento
    this.limpiarCampos();
    this.lanzarEvento();
  }

  showError(error){
    console.log("Hubo un error en nuevo-usuario");
    console.log(error.detail.request.xhr.response.msg);    
    this.lanzarNotificacion(error.detail.request.xhr.response.msg);
  }

  lanzarNotificacion(texto){
    this.$.notification.renderer = function(root) {
              root.textContent = texto;
            };
    this.$.notification.hidden = false;
    this.$.notification.opened = true;
  }

  volver(){
    this.limpiarCampos();
    this.lanzarEvento();

  }

  lanzarEvento(){
    //lanzamos evento con iban
    console.log("Lanzamos evento desde nuevo-usuario");
    this.dispatchEvent(
          new CustomEvent(
            "new-user",
            {
              "detail" : {
                "userid": this.userid
              }//hasta aquí la estructura es fija
            }//object siempre es CustomEvent
          )//customEvent
        )//dispatch
  }

  limpiarCampos(){
    this.nombre = null;
    this.apellidos = null;
    this.email = null;
    this.password = null;
    this.passwordRequired = null;
  }

}//End class

window.customElements.define('nuevo-usuario', NuevoUsuario);
