import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-ajax/iron-ajax.js';
import '@vaadin/vaadin-combo-box/vaadin-combo-box.js';
import '@vaadin/vaadin-text-field/vaadin-text-field.js';
import '@vaadin/vaadin-button/vaadin-button.js';
import '@vaadin/vaadin-text-field/vaadin-number-field.js';
import '@vaadin/vaadin-notification/vaadin-notification.js';



/**
 * @customElement
 * @polymer
 */
class CambioDivisa extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block
        }
        .custom-style {
          width: 15em;
        }
      </style>

      <h3>Por favor, introduzca los datos</h3>

      <vaadin-combo-box id="comboDivisaOrigen"
        class="custom-style"
        label="Divisa origen"
        items="[[currencies]]"
        on-change="definirDivisaOrigen"
        required="true"
        item-label-path="currency"
        item-value-path="currency"
        >
      </vaadin-combo-box>
      <p> <span id="origenName"></span></p>
      <vaadin-number-field label="Importe (1.00-2000)"
        class="custom-style"
        value="{{importe::input}}"
        placeholder="00.00"
        pattern="[0-9]+([\.][0-9]+{0,2})?"
        step="0.01"
        min="1.00"
        max="2000"
        required="true"
        error-message="Introduzca un valor válido"
        clear-button-visible
      >
      </vaadin-number-field>
      </br>
      <vaadin-combo-box id="comboDivisaDestino"
        class="custom-style"
        label="Divisa destino"
        items="[[currencies]]"
        on-change="definirDivisaDestino"
        required="true"
        item-label-path="currency"
        item-value-path="currency"
        >
      </vaadin-combo-box>
      <p> <span id="destinoName"></span></p>

      <vaadin-text-field label="Cambio"
        class="custom-style"
        value="[[cambio]]"
        readonly="true"
      >
      </vaadin-text-field>
      <br/>
      <vaadin-button id="buttonConvertCurrency" on-click="solicitarCambio">Convertir</vaadin-button>
      <vaadin-button id="returnButton" on-click="volver">Volver</vaadin-button>
      <br/>
      <vaadin-notification id="notification"  hidden="true" position="top-stretch" duration="3000">
      </vaadin-notification>
      <iron-ajax
        id="getCurrencies"
        url="http://localhost:3000/backend_banco/currencies"
        handle-as="json"
        content-type="application/json"
        method="GET"
        on-response="manageAJAXresponseCurrencies"
        on-error="showErrorCurrencies"
      >
      </iron-ajax>
      <iron-ajax
        id="getCurrenciesConvert"
        url="http://localhost:3000/backend_banco/currencies/{{divisaOrigen}}/{{divisaDestino}}/{{importe}}"
        handle-as="json"
        content-type="application/json"
        method="GET"
        on-response="manageAJAXresponseCurrencyConverter"
        on-error="showErrorCurrencyConverter"
      >
      </iron-ajax>
    `;
  }
  static get properties() {
    return {
      currencies: {
        type: Array
      },
      divisaOrigen: {
        type: String
      },
      divisaDestino: {
        type: String
      },
      importe: {
        type: Number
      },
      cambio: {
        type: Number
      },
      updated: {
        type: Boolean,
        observer: "_updatedChanged"
      }
    };
  }// End properties

    solicitarCambio(){
      if(this.divisaOrigen!=null && this.divisaDestino!=null && this.importe!=null){
        console.log("Lanzamos petición para obtener divisas");
        var token = window.sessionStorage.getItem('token');
        this.$.getCurrenciesConvert.headers['Authorization'] = token;
        this.$.getCurrenciesConvert.generateRequest(); //lanza la petición
    }else{
      this.lanzarNotificacion("Por favor rellene todos los campos");
    }

  }

  definirDivisaOrigen(){
    console.log("El usuario ha seleccionado una divisa origen");
    this.divisaOrigen = this.$.comboDivisaOrigen.value;
    console.log(this.$.comboDivisaOrigen.selectedItem.currencyName);
    this.$.origenName.innerHTML  = this.$.comboDivisaOrigen.selectedItem.currencyName;
  }

  definirDivisaDestino(){
    console.log("El usuario ha seleccionado una divisa destino");
    this.divisaDestino = this.$.comboDivisaDestino.value;
    console.log(this.$.comboDivisaDestino.selectedItem.currencyName);
    this.$.destinoName.innerHTML  = this.$.comboDivisaDestino.selectedItem.currencyName;
  }

  _updatedChanged(newValue, oldValue){
    console.log("updated de cambio-divisa cambió");
    if(newValue === true){
      var token = window.sessionStorage.getItem('token');
      this.$.getCurrencies.headers['Authorization'] = token;
      this.$.getCurrencies.generateRequest();
      this.updated = false;
    }
  }

  manageAJAXresponseCurrencies(data){
    console.log("Llegaron los resultados de cambio-divisa: obtener divisas");
    this.currencies = data.detail.response;
  }

  showErrorCurrencies(error){
    console.log("Hubo un error obteniendo divisas");
    console.log(error.detail.request.xhr.response.msg);
    this.lanzarNotificacion(error.detail.request.xhr.response.msg);
    this.limpiarCampos();
  }

  //100,200 y 300
  manageAJAXresponseCurrencyConverter(data){
    console.log("Llegaron los resultados de cambio-divisa: cambio de divisa");
    this.cambio = data.detail.response.cambio;
  }

  showErrorCurrencyConverter(error){
    console.log("Hubo un error pidiendo cambio de divisa");
    console.log(error.detail.request.xhr.response.msg);    
    this.lanzarNotificacion(error.detail.request.xhr.response.msg);
    this.limpiarCampos();
  }

  volver(){
    this.limpiarCampos();
    this.lanzarEvento();
  }


    lanzarNotificacion(texto){
      this.$.notification.renderer = function(root) {
                root.textContent = texto;
              };
      this.$.notification.hidden = false;
      this.$.notification.opened = true;
    }

  lanzarEvento(){
    //lanzamos evento con accountid
    console.log("Lanzamos evento desde cambio-divisa");
    this.dispatchEvent(
          new CustomEvent(
            "return-cambio-divisa",
            {
              "detail" : {
              }//hasta aquí la estructura es fija
            }//object siempre es CustomEvent
          )//customEvent
        )//dispatch
  }

  limpiarCampos(){
    this.divisaOrigen = null;
    this.divisaDestino = null;
    this.importe = null;
    this.cambio = null;
    this.updated = false;
    this.$.comboDivisaOrigen.selectedItem = "";
    this.$.comboDivisaDestino.selectedItem = "";
  }

}//End class

window.customElements.define('cambio-divisa', CambioDivisa);
