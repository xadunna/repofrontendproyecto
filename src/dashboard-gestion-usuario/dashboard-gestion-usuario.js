import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-ajax/iron-ajax.js';
import '@polymer/iron-pages/iron-pages.js';
import '../login-usuario/login-usuario.js';
import '../nuevo-usuario/nuevo-usuario.js';
import '@vaadin/vaadin-button/vaadin-button.js';





/**
 * @customElement
 * @polymer
 */
class DashboardGestionUsuario extends PolymerElement {
  static get template() {
    return html`

      <iron-pages selected="[[componentName]]" attr-for-selected="component-name">
        <div id="loginUsuario" component-name="login-usuario">
          <login-usuario on-login="processLoginSuccess">
          </login-usuario>
        </div>
        <div component-name="nuevo-usuario">
          <nuevo-usuario on-new-user="processNewUser">
          </nuevo-usuario>
          </div>
      </iron-pages>
      <vaadin-button id="createUserButton" on-click="createUser">Registrarse</vaadin-button>
    `;
  }

  static get properties() {
    return {
      componentName: {
        type: String,
        value: "login-usuario"
      }
    };
  }// End properties

  processLoginSuccess(e){
    console.log("Capturado evento de login-usuario");
    console.log("Lanzamos envento al dashboard de usuario");
    this.dispatchEvent(
      new CustomEvent(
        "login-success",
        {
          "detail" : {
            "userid": e.detail.idusuario
          }//hasta aquí la estructura es fija
        }//object siempre es CustomEvent
      )//customEvent
    )//dispatch
  }

  processNewUser(e){
    console.log("Capturado evento de nuevo-usuario");
    //Vamos a ventana de login-usuario
    this.componentName = "login-usuario";
    this.$.createUserButton.hidden = false;

  }

  createUser(){
    this.componentName = "nuevo-usuario";
    this.$.createUserButton.hidden = true;
  }

}//End class

window.customElements.define('dashboard-gestion-usuario', DashboardGestionUsuario);
